<?php
/*
 * Plugin Name: Daily Green
 * Plugin URI: http://kanomdonut.pw
 * Description: --
 * Version: 1.0
 * Author: Kamontham Peson
 * Author URI: http://kanomdonut.pw
 * License: --
 * */

//If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
** The code that runs during plugin activation.
**/
require_once plugin_dir_path( __FILE__ ) . 'includes/activator.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/AdminPage.php';
?>
