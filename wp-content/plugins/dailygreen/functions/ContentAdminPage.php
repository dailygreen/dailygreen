<?php
function ContentDailyGreenPage(){
	ob_start();
	?>
	<h1>Daily Green</h1>
	<?php
	$html = ob_get_contents();
	ob_clean();
	return $html;
}
function ContentDailyGreenSettingPage(){
	ob_start();
	?>
	<h1>Daily Green Settings</h1>
	<?php
	$html = ob_get_contents();
	ob_clean();
	return $html;
}