<?php
require_once plugin_dir_path( __FILE__ ) . '../functions/ContentAdminPage.php';
add_action( 'admin_menu', array('AdminPage','DailyGreen_menu') );
class AdminPage{
	public function __construct(){

	}
	private function addAdminPage(){

	}
	public function DailyGreen_menu() {
		add_menu_page('DailyGreenTitle', 'DailyGreen', 'manage_options', 'DailyGreenPage', array('AdminPage','DailyGreen_menu_function'));
		add_submenu_page( 'DailyGreenPage','DailyGreenSetting', 'Settings', 'read','DailyGreenSettingPage', array('AdminPage','DailyGreenSettings'));
	}
	public function DailyGreen_menu_function() {
		echo ContentDailyGreenPage();
	}
	public function DailyGreenSettings() {
		echo ContentDailyGreenSettingPage();
	}
}


	