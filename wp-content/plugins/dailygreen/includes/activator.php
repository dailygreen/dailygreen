<?php

/**
 * Fired during plugin activation
 *
 * @link:       http://kanomdonut.pw
 * @since      1.0.0
 *
 * @package    DailyGreen
 * @subpackage DailyGreen/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    DailyGreen
 * @subpackage DailyGreen/includes
 * @author:       DailyGreen <kanomdonut>
 */
class Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
